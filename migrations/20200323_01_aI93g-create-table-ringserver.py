"""
Create table ringserver
"""

from yoyo import step

__depends__ = {}

steps = [
    step("CREATE TABLE ringserver_events (time timestamp, bytes integer, network VARCHAR(6), station VARCHAR(6), location VARCHAR(2), channel VARCHAR(3), city text, country text, agent text, geohash VARCHAR(12), client TEXT )",
         "DROP TABLE ringserver_events"
    )
]
