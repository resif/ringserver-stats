"""
Create continuous agregation query
"""

from yoyo import step

__depends__ = {'20200323_01_aI93g-create-table-ringserver'}

steps = [
    step("""
CREATE VIEW ringserver
WITH (timescaledb.continuous)
AS
SELECT
  time_bucket('1 day', time) as bucket,
  network, station, location, channel, country, client,
  sum(bytes) as bytes_sum,
  count(*) as connections
FROM
  ringserver_events
GROUP BY bucket, network, station, location, channel, country, client;
""",
        "DROP VIEW ringserver CASCADE")
]
