FROM python:3.8-slim
MAINTAINER RESIF DC <resif-dc@univ-grenoble-alpes.fr>

RUN pip3 install --no-cache-dir gunicorn
COPY requirements.txt /
RUN pip3 install --no-cache-dir -r /requirements.txt
COPY ringserverstats/* /app/
WORKDIR /app
ENTRYPOINT ["gunicorn", "-b", "0.0.0.0:8000"]
CMD ["webapp"]
