LOGDIR=$1
export INFLUXDB_HOST=osug-influxdb.u-ga.fr
export INFLUXDB_PORT=8086
export INFLUXDB_USER=sandboxer
export INFLUXDB_DB=sandbox
export INFLUXDB_SSL=yes
export INFLUXDB_PASS=sandboxer

# insersion des événements existant jour par jour
# mois par mois, suppression des événements

for y in 3 4 5 6 7 8; do
  for m in $(seq -f "%02g" 1 12); do
    for log in $(ls $LOGDIR/txlog-201$y$m*); do
      echo "=== $log ==="
      [[ -f $log ]] && ringserverstats --resample $log
    done
    influx -database $INFLUXDB_DB -host $INFLUXDB_HOST -ssl -unsafeSsl -username $INFLUXDB_USER -password $INFLUXDB_PASS -execute 'delete from ringserverevents'
  done
done
